# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Role.create(name: :admin)
Role.create(name: :inventory_manager)
Role.create(name: :sales_manager)
Role.create(name: :finance_manager)
Role.create(name: :sales_person)
user1 = User.create(email: 'dealershipowner@mail.com', password: '123456', password_confirmation: '123456')
user1.add_role(:admin)
user2 = User.create(email: 'inventorymanager@mail.com', password: '123456', password_confirmation: '123456')
user2.add_role(:inventory_manager)
user3 = User.create(email: 'salesmanager@mail.com', password: '123456', password_confirmation: '123456')
user3.add_role(:sales_manager)
user4 = User.create(email: 'financemanager@mail.com', password: '123456', password_confirmation: '123456')
user4.add_role(:finance_manager)
user5 = User.create(email: 'salesperson.mail@com', password: '123456', password_confirmation: '123456')
user5.add_role(:sales_person)