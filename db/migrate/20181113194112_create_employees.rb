class CreateEmployees < ActiveRecord::Migration[5.2]
  def change
    create_table :employees do |t|
      t.string :first_name
      t.string :last_name
      t.string :address
      t.string :city
      t.string :state
      t.string :zip_code
      t.string :phone
      t.string :email
      t.string :password
      t.string :dob
      t.string :position
      t.string :ec_name
      t.string :ec_phone

      t.timestamps
    end
  end
end
