class CreateQuotes < ActiveRecord::Migration[5.2]
  def change
    create_table :quotes do |t|
      t.date :date
      t.decimal :car_sale_price
      t.decimal :interest_rate
      t.integer :pay_period
      t.boolean :sold
      t.references :employee, foreign_key: true
      t.references :car, foreign_key: true
      t.references :customer, foreign_key: true

      t.timestamps
    end
  end
end
