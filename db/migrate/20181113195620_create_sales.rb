class CreateSales < ActiveRecord::Migration[5.2]
  def change
    create_table :sales do |t|
      t.date :data
      t.references :quote, foreign_key: true
      t.decimal :sales_tax
      t.decimal :total_price
      t.string :payment_method

      t.timestamps
    end
  end
end
