class CreateCars < ActiveRecord::Migration[5.2]
  def change
    create_table :cars do |t|
      t.string :vehicle_id
      t.string :color
      t.string :model
      t.string :make
      t.integer :year
      t.decimal :wholesale_price
      t.binary :image

      t.timestamps
    end
  end
end
