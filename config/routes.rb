Rails.application.routes.draw do
  root 'dashboard#index'
  devise_for :users
  resources :sales
  resources :quotes
  resources :cars
  resources :employees
  resources :customers
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
