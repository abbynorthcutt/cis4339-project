require 'test_helper'

class QuotesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @quote = quotes(:one)
  end

  test "should get index" do
    get quotes_url
    assert_response :success
  end

  test "should get new" do
    get new_quote_url
    assert_response :success
  end

  test "should create quote" do
    assert_difference('Quote.count') do
      post quotes_url, params: { quote: { car_id: @quote.car_id, car_sale_price: @quote.car_sale_price, customer_id: @quote.customer_id, date: @quote.date, employee_id: @quote.employee_id, interest_rate: @quote.interest_rate, pay_period: @quote.pay_period, sold: @quote.sold } }
    end

    assert_redirected_to quote_url(Quote.last)
  end

  test "should show quote" do
    get quote_url(@quote)
    assert_response :success
  end

  test "should get edit" do
    get edit_quote_url(@quote)
    assert_response :success
  end

  test "should update quote" do
    patch quote_url(@quote), params: { quote: { car_id: @quote.car_id, car_sale_price: @quote.car_sale_price, customer_id: @quote.customer_id, date: @quote.date, employee_id: @quote.employee_id, interest_rate: @quote.interest_rate, pay_period: @quote.pay_period, sold: @quote.sold } }
    assert_redirected_to quote_url(@quote)
  end

  test "should destroy quote" do
    assert_difference('Quote.count', -1) do
      delete quote_url(@quote)
    end

    assert_redirected_to quotes_url
  end
end
