class Quote < ApplicationRecord
  belongs_to :employee
  belongs_to :car
  belongs_to :customer
  has_one :sale
end
