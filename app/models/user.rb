class User < ActiveRecord::Base
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  #attr_accessible :email, :password, :password_confirmation, :remember_me

  def admin?
    has_role?(:admin)
  end
  def inventory_manager?
    has_role?(:inventory_manager)
  end
  def sales_manager?
    has_role?(:sales_manager)
  end
  def finance_manager?
    has_role?(:finance_manager)
  end
  def sales_person?
    has_role?(:sales_person)
  end
end
