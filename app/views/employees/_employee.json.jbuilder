json.extract! employee, :id, :first_name, :last_name, :address, :city, :state, :zip_code, :phone, :email, :password, :dob, :position, :ec_name, :ec_phone, :created_at, :updated_at
json.url employee_url(employee, format: :json)
