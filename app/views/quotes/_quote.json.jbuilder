json.extract! quote, :id, :date, :car_sale_price, :interest_rate, :pay_period, :sold, :employee_id, :car_id, :customer_id, :created_at, :updated_at
json.url quote_url(quote, format: :json)
