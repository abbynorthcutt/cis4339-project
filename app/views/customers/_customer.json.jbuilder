json.extract! customer, :id, :first_name, :last_name, :address, :city, :state, :zip_code, :phone, :email, :dob, :dl_number, :insurance, :created_at, :updated_at
json.url customer_url(customer, format: :json)
