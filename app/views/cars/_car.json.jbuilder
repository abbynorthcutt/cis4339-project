json.extract! car, :id, :vehicle_id, :color, :model, :make, :year, :wholesale_price, :image, :created_at, :updated_at
json.url car_url(car, format: :json)
