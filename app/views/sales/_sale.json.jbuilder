json.extract! sale, :id, :data, :quote_id, :sales_tax, :total_price, :payment_method, :created_at, :updated_at
json.url sale_url(sale, format: :json)
